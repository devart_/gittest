CREATE TABLE w.t1 (
  "ID" NUMBER(*,0) NOT NULL,
  "D" VARCHAR2(30 BYTE),
  CONSTRAINT pk_t1_id PRIMARY KEY ("ID")
);