﻿CREATE TABLE [dbo].[t_table] (
  [c1] [int] NULL,
  [c2] [int] NULL,
  [c3] [int] NULL,
  [c4] [int] NULL,
  [c5] [int] NULL,
  [c6] [int] NULL
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Somethign', 'SCHEMA', N'dbo', 'TABLE', N't_table'
GO